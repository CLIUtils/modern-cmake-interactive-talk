# modern-cmake-interactive-talk

A talk first given for PICSciE, then the HSF packaging group.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/CLIUtils%2Fmodern-cmake-interactive-talk/master?urlpath=lab/tree/CMakePICSciE.ipynb)


To run locally, install miniconda, download this repository, and then run:

```bash
conda env create
conda activate cmake-talk
jupyter lab
```

To convert to slides:

```bash
jupyter nbconvert CMakeSandCroundtable.ipynb --to slides
docker run  --rm -t -v $PWD:/slides astefanutti/decktape generic --key=" " --size 1280x768 CMakeSandCroundtable.slides.html CMakeSandCroundtable.slides.pdf
```
