{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## CMake: Best Practices\n",
    "#### Henry Schreiner\n",
    "\n",
    "Software & Computing Roundtable 2021-2-2\n",
    "\n",
    "<img alt=\"IRIS-HEP\" src=\"iris-hep-logo.png\" style=\"float:right;\"/>\n",
    "\n",
    "Links: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/CLIUtils%2Fmodern-cmake-interactive-talk/master?urlpath=lab/tree/CMakeHSF.ipynb)\n",
    "\n",
    "* The book: [cliutils.gitlab.io/modern-cmake](https://cliutils.gitlab.io/modern-cmake)\n",
    "* My blog: [iscinumpy.gitlab.io](https://iscinumpy.gitlab.io)\n",
    "* The workshop: [hsf-training.github.io/hsf-training-cmake-webpage](https://hsf-training.github.io/hsf-training-cmake-webpage/)\n",
    "* This talk: [gitlab.com/CLIUtils/modern-cmake-interactive-talk](https://gitlab.com/CLIUtils/modern-cmake-interactive-talk)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Intro to CMake"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "cmake --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## What is CMake?\n",
    "\n",
    "Is it a build system?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Build system example (Rake):\n",
    "\n",
    "```ruby\n",
    "# 01-rake/Rakefile\n",
    "\n",
    "task default: [:hello_world] do                       # hello_world.c \n",
    "     puts 'All built'                                 #      ↓\n",
    "end                                                   # hello_world\n",
    "                                                      #      ↓\n",
    "file hello_world: ['hello_world.c'] do |t|            # default task\n",
    "    sh \"gcc #{t.prerequisites.join(' ')} -o #{t.name}\"\n",
    "end\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "(cd 01-rake && rake)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Features:\n",
    "* Understands **when** to build/rebuild\n",
    "* Doesn't understand **how** to build\n",
    "* Generic; can be used for anything\n",
    "\n",
    "### Examples\n",
    "* `make`: Classic, custom syntax\n",
    "* `rake`: Ruby make\n",
    "* `ninja`: Google's entry, not designed to be hand written"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Build system generator\n",
    "\n",
    "* Understands the files you are building\n",
    "* System independent\n",
    "* You give relationships\n",
    "* Can find libraries, etc\n",
    "\n",
    "CMake is **two-stage**; the configuration step runs CMake, the build step runs a build-system (`make`, `ninja`, IDE, etc).\n",
    "\n",
    "> Aside: Modern CMake can run the install step directly without invoking the build system agian."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Build system generator example (CMake):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cmake\n",
    "# 01-rake/CMakeLists.txt\n",
    "cmake_minimum_required(VERSION 3.11)\n",
    "\n",
    "project(HelloWorld)\n",
    "\n",
    "add_executable(hello_world hello_world.c)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 01-rake -B 01-build\n",
    "cmake --build 01-build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### C/C++ Examples\n",
    "* `cmake`: Cross-platform Make (also Fortran, CUDA, C#, Swift)\n",
    "* `scons`: Software Carpentry Construction (Python)\n",
    "* `meson`: Newer Python entry\n",
    "* `basel`: Google's build system\n",
    "* Other languages often have *their own build system* (Rust, Go, Python, ...)\n",
    "\n",
    "> We will follow common convention and call these \"build-systems\" for short from now on"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Why CMake?\n",
    "\n",
    "![Interest over time](IntrestOverTime.png)\n",
    "\n",
    "\n",
    "It has become a standard\n",
    "\n",
    "* Approximately all IDEs support it\n",
    "* Many libraries have built-in support\n",
    "* Integrates with almost everything"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Custom Buildsystems are going away\n",
    "\n",
    "* [Qt 6 dropped QMake](https://blog.qt.io/blog/2019/08/07/technical-vision-qt-6/) for CMake (note: C++17 only too)\n",
    "* Boost is starting to support CMake at a reasonable level along with BJam\n",
    "* Standout: Google is dual supporting Bazel and CMake"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Recent highlights\n",
    "\n",
    "* Thrust just received a major CMake overhaul\n",
    "* TBB / Intel PSTL nicely support CMake in recent years\n",
    "* Pybind11's CMake support was ramped up in 2.6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# (More) Modern CMake\n",
    "\n",
    "* CMake is a new language to learn (and is a bit odd)\n",
    "* Classic CMake (CMake 2.8, from 2009) was ugly and had problems, but that's not Modern CMake!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* *Modern CMake* and [*More Modern CMake*](https://github.com/Bagira80/More-Modern-CMake/blob/master/MoreModernCMake.pdf)!\n",
    "    * CMake 3.0 in 2014: Modern CMake begins\n",
    "    * CMake 3.1-3.4 had important additions/fixes\n",
    "    * CMake 3.12 in mid 2018 completed the \"More Modern CMake\" phase\n",
    "    * Current CMake is 3.19 (2.20 in rc2 phase)\n",
    "* Eras of CMake\n",
    "    * Classic CMake: Directory based\n",
    "    * Modern CMake: Target based\n",
    "    * More Modern CMake: Unified target behavior\n",
    "    * CURRENT: Powerful CLI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Minimum Version is important!\n",
    "\n",
    "CMake has a (AFAIK) unique version system.\n",
    "\n",
    "If a file start with this:\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.0)\n",
    "```\n",
    "\n",
    "Then CMake will set all *policies* (which cover all *behavior* changes) to their 3.0 settings. This in theory means it is extremely backward compatible; upgrading CMake will not break or change anything at all. In practice, all the behavior changes had very good reasons to change, so this will be much buggier and less useful than if you set it higher!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You can also do this:\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.4...3.14)\n",
    "```\n",
    "\n",
    "Then\n",
    "\n",
    "* CMake < 3.4 will be an error\n",
    "* CMake 3.4 -- 3.11 will set 3.4 policies (feature was introduced in 3.12, but syntax is valid)\n",
    "* CMake 3.12 -- 3.14 will set current policies\n",
    "* CMake 3.15+ will set 3.14 policies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "* **Don't**: set this low without a range - it will harm users. Setting < 3.9 will break IPO, for example. Often in a way that can't be fixed by superprojects.\n",
    "* **Do**: set the highest minimum you can (build systems are hard/ugly enough as it is)\n",
    "* **Do**: test with the lowest version version you support in at least one job.\n",
    "* **Don't**: expect a CMake version significanly _older_ than your compiler to work with it (expecially macOS/Windows, or CUDA)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### What minimum to choose - OS support:\n",
    "\n",
    "* 3.4: The bare minimum. Never set less.\n",
    "* 3.7: Debian old-stable.\n",
    "* 3.10: Ubuntu 18.04.\n",
    "* 3.11: CentOS 8 (use EPEL or AppSteams, though)\n",
    "* 3.13: Debian stable.\n",
    "* 3.16: Ubuntu 20.04.\n",
    "* 3.18: pip\n",
    "* 3.19: conda-forge/chocolaty/direct download, etc. First to support Apple Silicon.\n",
    "\n",
    "#### What minimum to choose - Features:\n",
    "\n",
    "* 3.8: C++ meta features, CUDA, lots more\n",
    "* 3.11: IMPORTED INTERFACE setting, faster, FetchContent, COMPILE_LANGUAGE in IDEs\n",
    "* 3.12: C++20, `cmake --build build -j N`, `SHELL:`, FindPython\n",
    "* 3.14/3.15: CLI, FindPython updates\n",
    "* 3.16: Unity builds / precompiled headers, CUDA meta features\n",
    "* 3.17/3.18: Lots more CUDA, metaprogramming"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Running CMake\n",
    "\n",
    "The classic method:\n",
    "```cmake\n",
    "mkdir build\n",
    "cd build\n",
    "cmake ..\n",
    "make\n",
    "```\n",
    "\n",
    "\n",
    "The modern method is cleaner and more cross-platform-friendly:\n",
    "```cmake\n",
    "cmake -S . -B build\n",
    "cmake --build build\n",
    "```\n",
    "\n",
    "(CMake 3.14/3.15) supports `-v` (verbose), `-j N` (threads), `-t target`, and more"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Example options:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmake --build 01-build -v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "<img src=\"04-mermaid-libs.svg\" alt=\"Library dependency example\" width=\"400\" style=\"float:right;\">\n",
    "\n",
    "## Best Practice: Use Targets\n",
    "\n",
    "Excutables and libraries are *targets*\n",
    "\n",
    "* Targets have private and interface properties\n",
    "* Targets can depend on each other `PRIVATE`ly or `PUBLIC`ally, or as an `INTERFACE`\n",
    "* An `IMPORTED` target is built by someone else\n",
    "* An `INTERFACE` target is not built\n",
    "\n",
    "\n",
    "```cmake\n",
    "add_library(MyLibrary mylibrary.cpp mylibrary.h)\n",
    "target_link_libraries(MyLibrary PRIVATE OpenMP::OpenMP_CXX) # OpenMP CMake 3.9+\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Properties include:\n",
    "\n",
    "* Header include directories\n",
    "* Compile flags and definitions\n",
    "* Link flags\n",
    "* C++ standard and/or compiler features required\n",
    "* Linked libraries\n",
    "\n",
    "Note that other things include properties, like files (such as LANGUAGE), directories, and global"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Tips for packaging\n",
    "\n",
    "* **Do**: use targets.\n",
    "* **Don't**: use common names for targets if you want to be used in subdirectory mode.\n",
    "* **Don't**: write a FindPackage for your own package. Always provide a PackageConfig instead.\n",
    "\n",
    "Export your targets to create a `PackageTargets.cmake` file. You can write a `PackageConfig.cmake.in`; you can recreate / reimport package there (generally use a shared `X.cmake` file instead of doing it twice).\n",
    "\n",
    "* **Don't**: hardcode any system/compiler/config details into exported targets. Only from shared code in Config, or use Generator expressions.\n",
    "\n",
    "<span style=\"color:red;\">Never allow your package to be distributed without the config files!</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### Using other projects\n",
    "\n",
    "1. See if the project provides a `<name>Config.cmake` file. If it does, you are good to go!\n",
    "2. If not, see if CMake [provides a Find package](https://cmake.org/cmake/help/latest/manual/cmake-modules.7.html) for it built-in. If it does, you are good to go!\n",
    "3. If not, see if the authors provide a `Find<name>.cmake` file. If they do, complain loudly that they don't follow CMake best practices.\n",
    "4. Write your own `Find<name>.cmake` and include in in a helper directory. You'll need to build IMPORTED targets and all that.\n",
    "5. You can also use `FindPkgConfig` to piggy back on classic pkg-config."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Handling remote dependencies\n",
    "\n",
    "CMake can download your dependencies for you, and can integrate with files. It supports composable (sub-)projects: One project can include another\n",
    " \n",
    "<span style=\"color:red;\">Does not have namespaces, can cause target collisions!</span>\n",
    "\n",
    "* Build time data and project downloads: `ExternalProject` (classic)\n",
    "* Configure time downloads `FetchContent` (new in 3.11+)\n",
    "* You can also use submodules (one of my favorite methods, but use with care)\n",
    "* You can also use Conan.io's CMake integration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "### FetchContent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "`02-fetch/hello_fmt.cpp`\n",
    "\n",
    "```c++\n",
    "#include <fmt/format.h>\n",
    "\n",
    "int main() {\n",
    "    fmt::print(\"The answer is {}\\n\", 42);\n",
    "    return 0;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "`02-fetch/CMakeLists.txt`\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.14)\n",
    "project(HelloWorld LANGUAGES CXX)\n",
    "```\n",
    "```cmake\n",
    "include(FetchContent)\n",
    "FetchContent_Declare(\n",
    "  fmt\n",
    "  GIT_REPOSITORY https://github.com/fmtlib/fmt.git\n",
    "  GIT_TAG        5.3.0)\n",
    "FetchContent_MakeAvailable(fmt) # Shortcut from CMake 3.14\n",
    "```\n",
    "```cmake\n",
    "add_executable(hello_world hello_fmt.cpp)\n",
    "target_link_libraries(hello_world PRIVATE fmt::fmt)\n",
    "target_compile_features(hello_world PRIVATE cxx_std_11)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 02-fetch -B 02-build\n",
    "cmake --build 02-build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "### Conan.io's conan-cmake\n",
    "\n",
    "Conan.io has a nice CMake integration tool. It _should_ support binaries too, since Conan.io supports them! You must have conan installed, though - I'm using conan from conda-forge. It works with old versions of CMake, as well.\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.14)\n",
    "\n",
    "project(HelloWorld LANGUAGES CXX)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```cmake\n",
    "# Conan bootstrap\n",
    "if(NOT EXISTS \"${CMAKE_BINARY_DIR}/conan.cmake\")\n",
    "  message(\n",
    "    STATUS\n",
    "      \"Downloading conan.cmake from https://github.com/conan-io/cmake-conan\")\n",
    "  file(DOWNLOAD \"https://github.com/conan-io/cmake-conan/raw/v0.16.1/conan.cmake\"\n",
    "       \"${CMAKE_BINARY_DIR}/conan.cmake\")\n",
    "endif()\n",
    "```\n",
    "```cmake\n",
    "include(\"${CMAKE_BINARY_DIR}/conan.cmake\")\n",
    "conan_check(REQUIRED)\n",
    "```\n",
    "```cmake\n",
    "conan_cmake_run(\n",
    "  REQUIRES fmt/6.1.2\n",
    "  BASIC_SETUP CMAKE_TARGETS\n",
    "  BUILD missing)\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```cmake\n",
    "\n",
    "add_executable(hello_world hello_fmt.cpp)\n",
    "target_link_libraries(hello_world PRIVATE CONAN_PKG::fmt)\n",
    "target_compile_features(hello_world PRIVATE cxx_std_11)\n",
    "\n",
    "\n",
    "```\n",
    "\n",
    "You can also make a `conanfile.txt` and manage all your dependencies there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 02b-conan -B 02b-build -DCMAKE_BUILD_TYPE=Release\n",
    "cmake --build 02b-build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Use IMPORTED targets for things you don't build\n",
    "* Now (3.11+) can be built with standard CMake commands!\n",
    "* Can now be global with `IMPORTED_GLOBAL`\n",
    "* You'll need to set them back up in your Config file (place in one common location for both CMakeLists and Config).\n",
    "\n",
    "```cmake\n",
    "add_library(ExternLib IMPORTED INTERFACE)\n",
    "\n",
    "# Classic                           # Modern\n",
    "set_property(                       target_include_directories(\n",
    "    TARGET                              ExternLib INTERFACE /my/inc\n",
    "      ExternLib                     )\n",
    "    APPEND\n",
    "    PROPERTY\n",
    "      INTERFACE_INCLUDE_DIRECTORIES\n",
    "        /my/inc\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Use CUDA as a language\n",
    "\n",
    "Cuda is now a first-class language in CMake! (3.9+) Replaces FindCuda.\n",
    "\n",
    "```cmake\n",
    "project(MY_PROJECT LANGUAGES CUDA CXX) # Super project might need matching? (3.14 at least)\n",
    "\n",
    "# Or for optional CUDA support\n",
    "project(MY_PROJECT LANGUAGES CXX)\n",
    "include(CheckLanguage)\n",
    "check_language(CUDA)\n",
    "if(CMAKE_CUDA_COMPILER)\n",
    "    enable_language(CUDA)\n",
    "endif()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Much like you can set C++ standards, you can set CUDA standards too:\n",
    "\n",
    "```cmake\n",
    "if(NOT DEFINED CMAKE_CUDA_STANDARD)\n",
    "    set(CMAKE_CUDA_STANDARD 11) # Probably should be cached!\n",
    "    set(CMAKE_CUDA_STANDARD_REQUIRED ON)\n",
    "endif()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "You can add files with `.cu` extensions and they compile with nvcc. (You can always set the LANGUAGE property on a file, too). Separable compilation is a property:\n",
    "\n",
    "```cmake\n",
    "set_target_properties(mylib PROPERTIES\n",
    "                            CUDA_SEPERABLE_COMPILATION ON)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### New for CUDA in CMake 3.18 and 3.17!\n",
    "\n",
    "* A new `CUDA_ARCHITECTURES` property\n",
    "* You can now use Clang as a CUDA compiler\n",
    "* `CUDA_RUNTIME_LIBRARY` can be set to shared\n",
    "* `FindCUDAToolkit` is finally available\n",
    "* I rewrote the CUDA versions support in 3.20 to be more maintainable and more accurate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Check for Integrated Tools First\n",
    "\n",
    "CMake has a _lot_ of great tools. When you need something, see if it is built-in first!\n",
    "\n",
    "Useful properties (with `CMAKE_*` variables, great from the command line_:\n",
    "* `INTERPROCEDURAL_OPTIMIZATION`: Add IPO\n",
    "* `POSITION_INDEPENDENT_CODE`: Add `-fPIC`\n",
    "* `<LANG>_COMPILER_LAUNCHER`: Add `ccache`\n",
    "* `<LANG>_CLANG_TIDY`\n",
    "* `<LANG>_CPPCHECK`\n",
    "* `<LANG>_CPPLINT`\n",
    "* `<LANG>_INCLUDE_WHAT_YOU_USE`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Useful modules:\n",
    "\n",
    "* `CheckIPOSupported`: See if IPO is supported by your compiler\n",
    "* `CMakeDependentOption`: Make one option depend on another\n",
    "* `CMakePrintHelpers`: Handy debug printing\n",
    "* `FeatureSummary`: Record or printout enabled features and found packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Using clang-tidy in GitHub Actions example `.github/workflow/format.yml`:\n",
    "\n",
    "```yaml\n",
    "on:\n",
    "  pull_request:\n",
    "  push:\n",
    "```\n",
    "```yaml\n",
    "jobs:\n",
    "  clang-tidy:\n",
    "    runs-on: ubuntu-latest\n",
    "    container: silkeh/clang:10\n",
    "    steps:\n",
    "    - uses: actions/checkout@v2\n",
    "```\n",
    "```yaml  \n",
    "    - name: Configure\n",
    "      run: cmake -S . -B build -DCMAKE_CXX_CLANG_TIDY=\"$(which clang-tidy);--warnings-as-errors=*\"\n",
    "    - name: Run\n",
    "      run: cmake --build build -j 2\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Best Practice: Use CMake-Format\n",
    "\n",
    "There is a CMake formatting tool now too, called `cmake-format`! You should use it to keep your CMake code from becoming messy and keeping it easy to merge conflicts.\n",
    "\n",
    "\n",
    "Since you should _always_ use pre-commit for formatting and style checking, here's the `.pre-commit-config.yaml`:\n",
    "\n",
    "```yaml\n",
    "# CMake formatting\n",
    "- repo: https://github.com/cheshirekow/cmake-format-precommit\n",
    "  rev: v0.6.13\n",
    "  hooks:\n",
    "  - id: cmake-format\n",
    "    additional_dependencies: [pyyaml]\n",
    "    types: [file]\n",
    "    files: (\\.cmake|CMakeLists.txt)(.in)?$\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "And here's the GitHub Actions job:\n",
    "\n",
    "```yaml\n",
    "jobs:\n",
    "  pre-commit:\n",
    "    name: Format\n",
    "    runs-on: ubuntu-latest\n",
    "    steps:\n",
    "    - uses: actions/checkout@v2\n",
    "    - uses: actions/setup-python@v2\n",
    "    - uses: pre-commit/action@v2.0.0\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### CompilerDetection and Flag checking\n",
    "\n",
    "`try_compile` / `try_run` can tell you if a flag or file works.  However, first check:\n",
    "\n",
    "* `CheckCXXCompilerFlag`\n",
    "* `CheckIncludeFileCXX`\n",
    "* `CheckStructHasMember`\n",
    "* `TestBigEndian`\n",
    "* `CheckTypeSize`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "#### ~~Best~~ Bad Practice: `WriteCompilerDetectionHeader` will write out C/C++ macros for your compiler for you!\n",
    "\n",
    "This has been removed in CMake 3.20. Feel free to set the minimum required to below 3.20, or find another tool, generate once, then keep the generated copy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "`03-compiler/CMakeLists.txt`\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.15)\n",
    "project(CompilerExample LANGUAGES CXX)\n",
    "\n",
    "include(WriteCompilerDetectionHeader)\n",
    "```\n",
    "```cmake\n",
    "write_compiler_detection_header(\n",
    "  FILE my_compiler_detection.h\n",
    "  PREFIX MyPrefix\n",
    "  COMPILERS\n",
    "    GNU Clang MSVC Intel\n",
    "  FEATURES\n",
    "    cxx_variadic_templates\n",
    "    cxx_nullptr\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 03-compiler -B 03-build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Best Practice: Use FindPython and the newest possible CMake (3.18.2+ best)\n",
    "\n",
    "FindPython is an exciting new way to discover Python.\n",
    "\n",
    "* venv/conda ready.\n",
    "* Multiple runs (using unique caching system).\n",
    "* Not very usable till 3.15, not very usable for PyPy until 3.18.2+ and PyPy 7.3.2 (about a week old)\n",
    "    * But possibly vendorable to 3.7+"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Scikit-build and CMake wheels\n",
    "\n",
    "Scikit-build is an adaptor for setuptools (not a true PEP 517 builder). Combined with `pyproject.toml` and Pip 10, it can be really useful, though!\n",
    "\n",
    "* Still not quite as reliable as setuptools (distutils) in non-standard setups\n",
    "* Doesn't work well without CMake wheels (but you can bypass PEP 518 if CMake is already installed)\n",
    "* A little rough around some corners\n",
    "* Development a bit stuck."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Further investigation:\n",
    "\n",
    "If we have some spare time, I can show you through the CMake systems I've helped design:\n",
    "\n",
    "* [github.com/pybind/pybind11](https://github.com/pybind/pybind11)\n",
    "* [github.com/pybind11/scikit_build_example](https://github.com/henryiii/scikit_build_example)\n",
    "* [github.com/cliutils/cli11](https://github.com/cliutils/cli11)\n",
    "* [github.com/scikit-hep/boost-histogram](https://github.com/scikit-hep/boost-histogram) (Dual CMake / setuptools)\n",
    "* [github.com/goofit/goofit](https://github.com/goofit/goofit) (CUDA and Scikit-Build)\n",
    "\n",
    "I highly recommend my course [hsf-training.github.io/hsf-training-cmake-webpage](https://hsf-training.github.io/hsf-training-cmake-webpage/) and book [cliutils.gitlab.io/modern-cmake](https://cliutils.gitlab.io/modern-cmake/) on CMake. Everything is linked from my blog, [iscinumpy.gitlab.io](https://iscinumpy.gitlab.io).\n",
    "\n",
    "Also, [CMake's documentation](https://cmake.org/cmake/help/latest/index.html) is fantastic, if you already know what you are looking for, it is some of the best out there. The \"new in\" directives seem to be finally added in the 3.20 documentation! No more scrolling though old versions!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
